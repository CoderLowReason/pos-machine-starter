package pos.machine;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class PosMachine {
    public String printReceipt(List<String> barcodes) {
        String itemsOutput = this.buildItemsOutput(ItemsLoader.loadAllItems(), this.mapBarcodeToQuantity(barcodes));
        return this.buildReceiptOutput(itemsOutput);
    }

    Map<String, Long> mapBarcodeToQuantity(List<String> barcodes) {
        return barcodes.stream().collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
    }

    List<Item> getItemsByBarcodes(List<String> barcodes) {
        System.out.println(barcodes); // assuming we have used barcodes to query items
        return ItemsLoader.loadAllItems();
    }

    Integer calculateSubtotal(Integer quantity, Integer price) {
        return quantity * price;
    }

    Integer calculateTotal(List<Integer> subtotals) {
        return subtotals.stream().mapToInt(Integer::intValue).sum();
    }

    String buildItemsOutput(List<Item> items, Map<String, Long> barcodeQuantityMap) {
        StringBuilder itemsOutputLines = new StringBuilder();
        List<Integer> subtotals = new ArrayList<>();
        items.stream().forEach(item -> {
            String barcode = item.getBarcode();
            int price = item.getPrice();
            String itemName = item.getName();
            Integer quantity = Math.toIntExact(barcodeQuantityMap.get(barcode));
            Integer subtotal = this.calculateSubtotal(quantity, price);
            String line = String.format("Name: %s, Quantity: %d, Unit price: %d (yuan), Subtotal: %d (yuan)\n", itemName, quantity, price, subtotal);
            itemsOutputLines.append(line);
            subtotals.add(subtotal);
        });
        itemsOutputLines.append("----------------------\n")
                .append(String.format("Total: %d (yuan)\n", this.calculateTotal(subtotals)));
        return itemsOutputLines.toString();
    }

    String buildReceiptOutput(String itemsOutputLines) {
        return "***<store earning no money>Receipt***\n" +
                    itemsOutputLines +
                "**********************";
    }
}
